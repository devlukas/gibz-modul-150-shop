<?php
namespace One50\Shop\I18n;

class Translator extends \TYPO3\Flow\I18n\Translator {
	
	/**
	 * @param string                       $labelId
	 * @param array                        $arguments
	 * @param null                         $quantity
	 * @param \TYPO3\Flow\I18n\Locale|null $locale
	 * @param string                       $sourceName
	 * @param string                       $packageKey
	 * @return string
	 */
	public function translateById($labelId, array $arguments = array(), $quantity = null, \TYPO3\Flow\I18n\Locale $locale = null, $sourceName = 'Main', $packageKey = 'One50.Shop') {
		return parent::translateById($labelId, $arguments, $quantity, $locale, $sourceName, $packageKey);
	}
	
}