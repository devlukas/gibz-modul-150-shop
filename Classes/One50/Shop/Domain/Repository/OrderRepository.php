<?php
namespace One50\Shop\Domain\Repository;

/*
 * This file is part of the One50.Shop package.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\QueryInterface;
use TYPO3\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class OrderRepository extends Repository {
	
	/**
	 * Default ordering by column "title" (ascending)
	 *
	 * @var array
	 */
	protected $defaultOrderings = array ('orderDate' => QueryInterface::ORDER_DESCENDING);
	
}
