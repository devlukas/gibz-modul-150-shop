<?php
namespace One50\Shop\Domain\Model;

/*
 * This file is part of the One50.Shop package.
 */
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Product {
	
	/**
	 * @var string
	 * @Flow\Validate(type="NotEmpty")
	 * @Flow\Validate(type="StringLength", options={"maximum"=120})
	 * @ORM\Column(length=120)
	 */
	protected $title;
	
	/**
	 * @var string
	 * @ORM\Column(length=1023)
	 */
	protected $description;
	
	/**
	 * @var float
	 * @Flow\Validate(type="NotEmpty")
	 * @Flow\Validate(type="Float")
	 */
	protected $price;
	
	/**
	 * @var \TYPO3\Flow\Resource\Resource
	 * @ORM\OneToOne
	 * @ORM\Column(nullable=true)
	 */
	protected $image;
	
	/**
	 * @var \One50\Shop\Domain\Model\Category
	 * @ORM\ManyToOne(inversedBy="products")
	 */
	protected $category;
	
	
	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}
	
	/**
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}
	
	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}
	
	/**
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}
	
	/**
	 * @return integer
	 */
	public function getPrice() {
		return $this->price;
	}
	
	/**
	 * @param integer $price
	 * @return void
	 */
	public function setPrice($price) {
		$this->price = $price;
	}
	
	/**
	 * @return \TYPO3\Flow\Resource\Resource
	 */
	public function getImage() {
		return $this->image;
	}
	
	/**
	 * @param \TYPO3\Flow\Resource\Resource $images
	 */
	public function setImage(\TYPO3\Flow\Resource\Resource $image) {
		$this->image = $image;
	}
	
	/**
	 * @return \One50\Shop\Domain\Model\Category
	 */
	public function getCategory() {
		return $this->category;
	}
	
	/**
	 * @param \One50\Shop\Domain\Model\Category $category
	 * @return void
	 */
	public function setCategory(\One50\Shop\Domain\Model\Category $category) {
		$this->category = $category;
	}
	
}
