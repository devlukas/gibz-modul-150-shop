<?php
namespace One50\Shop\Domain\Model;

/*
 * This file is part of the One50.Shop package.
 */

use One50\Shop\Domain\Model\OrderItem;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Order {
	
	const ORDER_STATUS_PENDING = 10;
	const ORDER_STATUS_PAID = 20;
	const ORDER_STATUS_COMPLETED = 50;
	
	/**
	 * @var \DateTime
	 */
	protected $orderDate;
	
	/**
	 * @var string
	 * @Flow\Validate(type="NotEmpty")
	 * @Flow\Validate(type="StringLength", options={"maximum"=20})
	 * @ORM\Column(length=20)
	 */
	protected $transactionId;
	
	/**
	 * @var integer
	 * @Flow\Validate(type="NotEmpty")
	 * @Flow\Validate(type="Integer")
	 */
	protected $status;
	
	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection<\One50\Shop\Domain\Model\OrderItem>
	 * @ORM\OneToMany(mappedBy="parentOrder", cascade={"all"})
	 */
	protected $items;
	
	/**
	 * @var \One50\Shop\Domain\Model\User
	 * @ORM\ManyToOne(inversedBy="orders")
	 */
	protected $user;
	
	/**
	 * @var \One50\Shop\Domain\Model\Address
	 * @ORM\ManyToOne
	 */
	protected $billingAddress;
	
	/**
	 * @var \One50\Shop\Domain\Model\Address
	 * @ORM\ManyToOne
	 */
	protected $deliveryAddress;
	
	/**
	 * @return mixed
	 */
	public function getOrderDate() {
		return $this->orderDate;
	}
	
	/**
	 * @param mixed $orderDate
	 */
	public function setOrderDate($orderDate) {
		$this->orderDate = $orderDate;
	}
	
	/**
	 * @return string
	 */
	public function getTransactionId() {
		return $this->transactionId;
	}
	
	/**
	 * @param string $transactionId
	 * @return void
	 */
	public function setTransactionId($transactionId) {
		$this->transactionId = $transactionId;
	}
	
	/**
	 * @return integer
	 */
	public function getStatus() {
		return $this->status;
	}
	
	/**
	 * @param integer $status
	 * @return void
	 */
	public function setStatus($status) {
		$this->status = $status;
	}
	
	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getItems() {
		return $this->items;
	}
	
	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection $items
	 */
	public function setItems($items) {
		$this->items = $items;
	}
	
	/**
	 * @param OrderItem $item
	 */
	public function addItem(OrderItem $item) {
		$this->items->add($item);
	}
	
	/**
	 * @param OrderItem $item
	 */
	public function removeItem(OrderItem $item) {
		$this->items->remove($item);
	}
	
	/**
	 * @return mixed
	 */
	public function getUser() {
		return $this->user;
	}
	
	/**
	 * @param mixed $user
	 */
	public function setUser($user) {
		$this->user = $user;
	}
	
	/**
	 * @return mixed
	 */
	public function getBillingAddress() {
		return $this->billingAddress;
	}
	
	/**
	 * @param mixed $billingAddress
	 */
	public function setBillingAddress($billingAddress) {
		$this->billingAddress = $billingAddress;
	}
	
	/**
	 * @return Address
	 */
	public function getDeliveryAddress() {
		return $this->deliveryAddress;
	}
	
	/**
	 * @param Address $deliveryAddress
	 */
	public function setDeliveryAddress($deliveryAddress) {
		$this->deliveryAddress = $deliveryAddress;
	}
	
	public function getTotalSum() {
		$sum = 0;
		/** @var OrderItem $orderItem */
		foreach ($this->getItems() as $orderItem) {
			$sum += $orderItem->getProduct()->getPrice() * $orderItem->getQuantity();
		}
		return $sum;
	}
}
