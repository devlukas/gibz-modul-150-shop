<?php
namespace One50\Shop\Domain\Model;

/*
 * This file is part of the One50.Shop package.
 */

use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Address {
	
	/**
	 * @var \One50\Shop\Domain\Model\User
	 * @ORM\ManyToOne(inversedBy="addresses")
	 */
	protected $user;
	
	/**
	 * @var string
	 * @Flow\Validate(type="NotEmpty")
	 * @Flow\Validate(type="StringLength", options={"maximum"=80})
	 * @ORM\Column(length=80)
	 */
	protected $street;
	
	/**
	 * @var string
	 * @Flow\Validate(type="StringLength", options={"maximum"=10})
	 * @ORM\Column(length=10, nullable=true)
	 */
	protected $streetNumber;
	
	/**
	 * @var integer
	 * @Flow\Validate(type="NotEmpty")
	 * @Flow\Validate(type="Integer")
	 * @Flow\Validate(type="NumberRange", options={"minimum"=1000, "maximum"=9999})
	 */
	protected $zip;
	
	/**
	 * @var string
	 * @Flow\Validate(type="NotEmpty")
	 * @Flow\Validate(type="StringLength", options={"maximum"=80})
	 * @ORM\Column(length=80)
	 */
	protected $city;
	
	/**
	 * @var string
	 * @Flow\Validate(type="NotEmpty")
	 * @Flow\Validate(type="EmailAddress")
	 * @Flow\Validate(type="StringLength", options={"maximum"=255})
	 * @ORM\Column(length=255)
	 */
	protected $email;
	
	/**
	 * @var string
	 * @Flow\Validate(type="StringLength", options={"maximum"=20})
	 * @ORM\Column(length=20)
	 */
	protected $phone;
	
	/**
	 * @return User
	 */
	public function getUser() {
		return $this->user;
	}
	
	/**
	 * @param User $user
	 */
	public function setUser($user) {
		$this->user = $user;
	}
	
	/**
	 * @return mixed
	 */
	public function getStreet() {
		return $this->street;
	}
	
	/**
	 * @param mixed $street
	 */
	public function setStreet($street) {
		$this->street = $street;
	}
	
	/**
	 * @return mixed
	 */
	public function getStreetNumber() {
		return $this->streetNumber;
	}
	
	/**
	 * @param mixed $streetNumber
	 */
	public function setStreetNumber($streetNumber) {
		$this->streetNumber = $streetNumber;
	}
	
	/**
	 * @return mixed
	 */
	public function getZip() {
		return $this->zip;
	}
	
	/**
	 * @param mixed $zip
	 */
	public function setZip($zip) {
		$this->zip = $zip;
	}
	
	/**
	 * @return mixed
	 */
	public function getCity() {
		return $this->city;
	}
	
	/**
	 * @param mixed $city
	 */
	public function setCity($city) {
		$this->city = $city;
	}
	
	/**
	 * @return mixed
	 */
	public function getEmail() {
		return $this->email;
	}
	
	/**
	 * @param mixed $email
	 */
	public function setEmail($email) {
		$this->email = $email;
	}
	
	/**
	 * @return string
	 */
	public function getPhone() {
		return $this->phone;
	}
	
	/**
	 * @param string $phone
	 */
	public function setPhone($phone) {
		$this->phone = $phone;
	}
	
}
