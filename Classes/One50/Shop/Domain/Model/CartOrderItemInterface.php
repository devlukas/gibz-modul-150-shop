<?php
namespace One50\Shop\Domain\Model;


interface CartOrderItemInterface {
	
	/**
	 * @return \One50\Shop\Domain\Model\Product
	 */
	public function getProduct();
	
	/**
	 * @return integer
	 */
	public function getQuantity();
	
}