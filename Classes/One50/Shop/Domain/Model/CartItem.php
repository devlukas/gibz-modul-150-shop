<?php
namespace One50\Shop\Domain\Model;

/*
 * This file is part of the One50.Shop package.
 */

use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class CartItem implements CartOrderItemInterface {
	
	/**
	 * @var \One50\Shop\Domain\Model\Cart
	 * @ORM\ManyToOne(inversedBy="items")
	 */
	protected $cart;
	
	/**
	 * @var \One50\Shop\Domain\Model\Product
	 * @ORM\ManyToOne
	 */
	protected $product;
	
	/**
	 * @var integer
	 * @Flow\Validate(type="NotEmpty")
	 * @Flow\Validate(type="Integer")
	 * @Flow\Validate(type="NumberRange", options={"minimum"=1})
	 */
	protected $quantity;
	
	
	/**
	 * @return \One50\Shop\Domain\Model\Cart
	 */
	public function getCart() {
		return $this->cart;
	}
	
	/**
	 * @param \One50\Shop\Domain\Model\Cart $cart
	 * @return void
	 */
	public function setCart(\One50\Shop\Domain\Model\Cart $cart) {
		$this->cart = $cart;
	}
	
	/**
	 * @return \One50\Shop\Domain\Model\Product
	 */
	public function getProduct() {
		return $this->product;
	}
	
	/**
	 * @param \One50\Shop\Domain\Model\Product $product
	 * @return void
	 */
	public function setProduct(\One50\Shop\Domain\Model\Product $product) {
		$this->product = $product;
	}
	
	/**
	 * @return integer
	 */
	public function getQuantity() {
		return $this->quantity;
	}
	
	/**
	 * @param integer $quantity
	 * @return void
	 */
	public function setQuantity($quantity) {
		$this->quantity = $quantity;
	}
	
}
