<?php
namespace One50\Shop\Validation\Validator;

class ExistsValidator extends \TYPO3\Flow\Validation\Validator\AbstractValidator {
	
	/**
	 * @var array
	 */
	protected $supportedOptions = array(
		'repository'   => array(null, 'Repository to look for the unique property', 'TYPO3\Flow\Persistence\RepositoryInterface', true),
		'propertyName' => array(null, 'name of the unique property', 'string', true),
	);
	
	/**
	 * @param mixed $value The value that should be validated
	 * @return void
	 * @throws \TYPO3\Flow\Validation\Exception\InvalidValidationOptionsException
	 */
	protected function isValid($value) {
		$repository = $this->options['repository'];
		if (!$repository instanceof \TYPO3\Flow\Persistence\RepositoryInterface) {
			throw new \TYPO3\Flow\Validation\Exception\InvalidValidationOptionsException('The option "repository" must implement RepositoryInterface.', 1336499435);
		}
		
		$propertyName = (string)$this->options['propertyName'];
		$query = $repository->createQuery();
		$numberOfResults = $query->matching($query->equals($propertyName, $value))->count();
		if ($numberOfResults === 0) {
			$this->addError('This %s was not found', 1340810986, array($propertyName));
		}
	}
}

?>