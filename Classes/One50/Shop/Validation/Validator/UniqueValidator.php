<?php
namespace One50\Shop\Validation\Validator;

class UniqueValidator extends \TYPO3\Flow\Validation\Validator\AbstractValidator {
	
	/**
	 * @var array
	 */
	protected $supportedOptions = array(
		'repository'   => array(null, 'Repository to look for the unique property', 'TYPO3\Flow\Persistence\RepositoryInterface', true),
		'propertyName' => array(null, 'name of the unique property', 'string', true),
		'alwaysAllow'  => array(null, 'if the unique property is equal to this option, the validator is disabled', 'mixed'),
	);
	
	/**
	 * @param mixed $value The value that should be validated
	 * @return void
	 * @throws \TYPO3\Flow\Validation\Exception\InvalidValidationOptionsException
	 */
	protected function isValid($value) {
		$repository = $this->options['repository'];
		if (!$repository instanceof \TYPO3\Flow\Persistence\RepositoryInterface) {
			throw new \TYPO3\Flow\Validation\Exception\InvalidValidationOptionsException('The option "repository" must implement RepositoryInterface.', 1336499435);
		}
		
		if (isset($this->options['alwaysAllow']) && $value === $this->options['alwaysAllow']) {
			return;
		}
		
		$propertyName = (string)$this->options['propertyName'];
		$query = $repository->createQuery();
		$numberOfResults = $query->matching($query->equals($propertyName, $value))->count();
		if ($numberOfResults > 0) {
			$this->addError("This {$this->options['propertyName']} is not unique", 1336499565);
		}
	}
}

?>