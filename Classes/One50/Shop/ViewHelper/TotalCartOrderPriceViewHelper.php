<?php
namespace One50\Shop\ViewHelper;


use Doctrine\Common\Collections\ArrayCollection;
use One50\Shop\Domain\Model\CartOrderItemInterface;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;

class TotalCartOrderPriceViewHelper extends AbstractViewHelper {
	
	/**
	 * Returns the total bill amount for a collection of cartItems/orderItems
	 *
	 * @param ArrayCollection $items
	 * @return int
	 */
	public function render(ArrayCollection $items) {
		$sum = 0;
		/** @var CartOrderItemInterface $item */
		foreach ($items as $item) {
			$sum += $item->getProduct()->getPrice() * $item->getQuantity();
		}
		return $sum;
	}
}