<?php
namespace One50\Shop\Controller;

/*
 * This file is part of the One50.Shop package.
 */

use One50\Shop\Domain\Model\Address;
use One50\Shop\Domain\Model\User;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Error\Message;
use TYPO3\Flow\Security\Account;

class UserController extends AbstractActionController {
	
	/**
	 * Translator
	 *
	 * @var \One50\Shop\I18n\Translator
	 * @Flow\Inject
	 */
	protected $translator;
	
	/**
	 * Account Factory
	 *
	 * @var \TYPO3\Flow\Security\AccountFactory
	 * @Flow\Inject
	 */
	protected $accountFactory;
	
	/**
	 * Account Repository
	 *
	 * @var \TYPO3\Flow\Security\AccountRepository
	 * @Flow\Inject
	 */
	protected $accountRepository;
	
	/**
	 * User Repository
	 *
	 * @var \One50\Shop\Domain\Repository\UserRepository
	 * @Flow\Inject
	 */
	protected $userRepository;
	
	/**
	 * Display a form for entering a new accounts data
	 *
	 * @param string $username
	 */
	public function registerAction($username = null) {
		// Just display the form
	}
	
	/**
	 * Create a new account and redirect to the second step (newUserAction)
	 *
	 * @param string $username
	 * @param string $password
	 * @param string $passwordRepetition
	 */
	public function createAccountAction($username, $password, $passwordRepetition) {
		// validate passwords
		if ($password !== $passwordRepetition) {
			$this->addFlashMessage(
				$this->translator->translateById('passwords.notIdentical'),
				'',
				Message::SEVERITY_ERROR
			);
			$this->redirect('register', null, null, array('username' => $username));
		}
		
		// validate username
		if ($this->accountRepository instanceof \TYPO3\Flow\Object\DependencyInjection\DependencyProxy) {
			$this->accountRepository->_activateDependency();
		}
		$arr_validatorOptions = array('repository' => $this->accountRepository, 'propertyName' => 'accountIdentifier');
		$uniqueValidator = $this->validatorResolver->createValidator('One50.Shop:Unique', $arr_validatorOptions);
		$result = $uniqueValidator->validate($username);
		
		if ($result->hasErrors()) {
			// username is not available
			$this->addFlashMessage(
				$this->translator->translateById('username.notAvailable.body', array($username)),
				$this->translator->translateById('username.notAvailable.title', array($username)),
				Message::SEVERITY_ERROR
			);
			$this->redirect('register', null, null, array('username' => $username));
		}
		
		// create account since username and password are valid
		$account = $this->accountFactory->createAccountWithPassword($username, $password, array(User::USER_ROLE_CUSTOMER));
		$this->accountRepository->add($account);
		
		$this->forward('new', null, null, array('account' => $account));
	}
	
	/**
	 * Display a form for entering a new users data
	 *
	 * @param Account $account
	 */
	public function newAction(Account $account) {
		$this->view->assign('account', $account);
	}
	
	/**
	 * Persist a new user
	 *
	 * @param User    $user
	 * @param Account $account
	 */
	public function createAction(User $user, Account $account) {
		$user->addAccount($account);
		$this->userRepository->add($user);
	}
	
	/**
	 * Display the users profile
	 *
	 * @param User $user
	 */
	public function showAction(User $user) {
		$this->view->assignMultiple(array('user'    => $user,
										  'account' => $user->getAccounts()->first())
		);
	}
	
	/**
	 * Display a form where the user may enter new address data
	 * @param User $user
	 */
	public function newAddressAction(User $user) {
		$this->view->assign('user', $user);
	}
	
	/**
	 * Add a new addres to the user, update the user and redirect to the show action
	 *
	 * @param Address $address
	 */
	public function createAddressAction(Address $address) {
		$user = $address->getUser();
		$user->addAddress($address);
		$this->userRepository->update($user);
		$this->redirect('show', null, null, array('user' => $user));
	}
	
	/**
	 * Display a form wehre the user may edit an address
	 *
	 * @param Address $address
	 */
	public function editAddressAction(Address $address) {
		$this->view->assign('address', $address);
	}
	
	/**
	 * Persist the changes for an address and redirect to the show action
	 *
	 * @param Address $address
	 */
	public function updateAddressAction(Address $address) {
		$this->persistenceManager->update($address);
		$this->redirect('show', null, null, array('user' => $address->getUser()));
	}
	
}
