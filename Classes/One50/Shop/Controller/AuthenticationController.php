<?php
namespace One50\Shop\Controller;

/*
 * This file is part of the One50.Shop package.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Security\Authentication\Controller\AbstractAuthenticationController;

class AuthenticationController extends AbstractAuthenticationController {
	
	/**
	 * @var \TYPO3\Party\Domain\Service\PartyService
	 * @Flow\Inject
	 */
	protected $partyService;
	
	/**
	 * Shopping Cart
	 *
	 * @var \One50\Shop\Service\Cart
	 * @Flow\Inject
	 */
	protected $cart;
	
	/**
	 * Category Repository
	 *
	 * @var \One50\Shop\Domain\Repository\CategoryRepository
	 * @Flow\Inject
	 */
	protected $categoryRepository;
	
	/**
	 * @param \TYPO3\Flow\Mvc\View\ViewInterface $view
	 */
	protected function initializeView(\TYPO3\Flow\Mvc\View\ViewInterface $view) {
		parent::initializeView($view);
		
		// assign categories to view
		$this->view->assign('categories', $this->categoryRepository->findAll());
		
		// assign cart to view
		$view->assign('cart', $this->cart);
	}
	
	/**
	 * Display a form where the users may enter their login credentials
	 *
	 * @return void
	 */
	public function indexAction() {
		// Just display the login form...
	}
	
	/**
	 * Redirect a successfully authenticated user
	 *
	 * @param \TYPO3\Flow\Mvc\ActionRequest|null $originalRequest
	 * @return string|void
	 */
	protected function onAuthenticationSuccess(\TYPO3\Flow\Mvc\ActionRequest $originalRequest = null) {
		if ($originalRequest !== NULL) {
			// redirect to original request, if there's one
			$this->redirectToRequest($originalRequest);
		} else {
			// redirect to users account instead
			$account = $this->securityContext->getAccount();
			/** @var \One50\Shop\Domain\Model\User $user */
			$user = $this->partyService->getAssignedPartyOfAccount($account);
			$this->redirect('show', 'User', null, array('user' => $user));
		}
	}
	
	/**
	 * Logout the currently logged in user and redirect to the login form
	 */
	public function logoutAction() {
		$this->authenticationManager->logout();
		$this->redirect('index');
	}
	
	
}
