<?php
namespace One50\Shop\Controller;

/*
 * This file is part of the One50.Shop package.
 */

use One50\Shop\Domain\Model\Order;
use One50\Shop\Domain\Model\User;
use TYPO3\Flow\Annotations as Flow;

class OrderController extends AbstractActionController {
	
	/**
	 * Order Repository
	 *
	 * @var \One50\Shop\Domain\Repository\OrderRepository
	 * @Flow\Inject
	 */
	protected $orderRepository;
	
	/**
	 * Display a list of all existing orders
	 */
	public function administrationAction() {
		$this->view->assign('orders', $this->orderRepository->findAll());
	}
	
	/**
	 * Display a list of all user orders
	 */
	public function indexAction() {
		/** @var User $user */
		$user = $this->user;
		$userOrders = $user->getOrders();
		$this->view->assign('orders', $userOrders);
	}
	
	/**
	 * Shows details information about a specific order
	 *
	 * @param Order  $order
	 * @param string $origin
	 */
	public function showAction(Order $order, $origin = '') {
		$this->view->assignMultiple(array('order'  => $order,
										  'origin' => $origin)
		);
	}
	
}
