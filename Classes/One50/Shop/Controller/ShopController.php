<?php
namespace One50\Shop\Controller;

/*
 * This file is part of the One50.Shop package.
 */

use One50\Shop\Domain\Model\CartItem;
use One50\Shop\Domain\Model\Category;
use One50\Shop\Domain\Model\Product;
use TYPO3\Flow\Annotations as Flow;

class ShopController extends AbstractActionController {
	
	/**
	 * Translator
	 *
	 * @var \One50\Shop\I18n\Translator
	 * @Flow\Inject
	 */
	protected $translator;
	
	/**
	 * Product Repository
	 *
	 * @var \One50\Shop\Domain\Repository\ProductRepository
	 * @Flow\Inject
	 */
	protected $productRepository;
	
	/**
	 * Category Repository
	 *
	 * @var \One50\Shop\Domain\Repository\CategoryRepository
	 * @Flow\Inject
	 */
	protected $categoryRepository;
	
	/**
	 * Shopping Cart
	 *
	 * @var \One50\Shop\Service\Cart
	 * @Flow\Inject
	 */
	protected $cart;
	
	/**
	 * @return void
	 */
	public function indexAction(Category $category = null) {
		if (isset($category)) {
			$products = $category->getProducts();
		} else {
			$products = $this->productRepository->findAll();
		}
		$this->view->assignMultiple(array('products'   => $products,
										  'category'   => $category)
		);
	}
	
	/**
	 * Add a product to the cart
	 *
	 * @param Product  $product
	 * @param Category $category
	 * @param integer  $quantity
	 */
	public function addToCartAction(Product $product, Category $category = null, $quantity = 1) {
		$item = new CartItem();
		$item->setProduct($product);
		$item->setQuantity($quantity);
		
		$this->cart->addItem($item);
		
		$this->addFlashMessage(
			$this->translator->translateById('addedItemToCart', array($quantity, $product->getTitle()), $quantity)
		);
		
		$this->redirect('index', null, null, array('category' => $category));
	}
	
}
