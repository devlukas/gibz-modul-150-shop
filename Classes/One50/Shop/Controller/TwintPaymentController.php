<?php
namespace One50\Shop\Controller;

/*
 * This file is part of the One50.Shop package.
 */


use One50\Twint\Service\PaymentService;
use One50\Shop\Domain\Model\Order;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Mvc\Routing;

use TYPO3\Flow as r;

class TwintPaymentController extends \TYPO3\Flow\Mvc\Controller\ActionController
{
	/**
	* @var One50\Twint\Service\PaymentService
	* @Flow\Inject
	*/
	protected $twintPaymentService;

	/**
	 * @var One50\Twint\Service\SoapService
	 * @Flow\Inject
	 */
	protected $twintSoapService;


    /**
	 * Starts a new payment with twint
	 *
	 * @return void
	 */
	public function startPaymentAction(Order $order) 
	{
		//start payment on twint
		$response = $this->twintPaymentService->startPayment($order);


		//if the service returned a token
		if (isset($response->Token)) 
		{
			$this->view->assignMultiple(array(
				'order'              => $order,
				'totalsum'			 => strval($order->getTotalSum()),
				'twintToken'         => str_split($response->Token),
				'twintQrCode' 	     => $response->QRCode,
				'twintPairingUuid'   => $response->CheckInNotification->PairingUuid,
			));
		}
		else if(isset($response->OrderUuid)) {
			$this->redirect('acceptOrder', "TwintPayment", null, array('order' => $order, 'orderUuid' => $response->OrderUuid));
		}
		//TODO: implement error
	}

	/**
	 * Checks the Pairing-Status by the pairing uuid
	 * @param Order $order
	 * @param string $orderUuid
	 * @return void
	 */
	public function acceptOrderAction(Order $order, $orderUuid){
		$this->view->assignMultiple(array(
				'order'              => $order,
				'orderUuid'          => $orderUuid,
			));
	}

    /**
	 * Checks the Pairing-Status by the pairing uuid
	 * @param string $pairingUuid
	 * @return mixed
	 */
	public function checkPairingStatusAction($pairingUuid)
	{
		return $this->twintPaymentService->checkPairing($pairingUuid);
	}
	
    /**
	 * Continues the payment and starts the order on twint
	 * @param string $pairingUuid
	 * @param Order $order
	 * @return mixed	 
	 */	
	public function continuePaymentAction($pairingUuid, $order)
	{
		//start twint order
		$response = $this->twintPaymentService->startOrder($order, $pairingUuid);

		//save order-mapping
		//$this->$twintPaymentService->createOrderMapping($order, $response->OrderUuid);

		return $response->OrderUuid;
	}

	/**
	 * Continues the payment and starts the order on twint
	 * @param string $orderUuid
	 * @return mixed	 
	 */	
	public function checkOrderStatusAction($orderUuid)
	{
		$orderStatus = $this->twintSoapService->monitorOrderStatus($orderUuid);
		return $orderStatus->Order->Status->Status->_;
	}

}
