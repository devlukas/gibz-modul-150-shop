<?php
namespace One50\Shop\Controller;

/*
 * This file is part of the One50.Shop package.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Security\Account;

abstract class AbstractActionController extends \TYPO3\Flow\Mvc\Controller\ActionController {
	
	/**
	 * Shopping Cart
	 *
	 * @var \One50\Shop\Service\Cart
	 * @Flow\Inject
	 */
	protected $cart;
	
	/**
	 * Security Context
	 *
	 * @var \TYPO3\Flow\Security\Context
	 * @Flow\Inject
	 */
	protected $securityContext;
	
	/**
	 * Party Service
	 *
	 * @var \TYPO3\Party\Domain\Service\PartyService
	 * @Flow\Inject
	 */
	protected $partyService;
	
	/**
	 * Category Repository
	 *
	 * @var \One50\Shop\Domain\Repository\CategoryRepository
	 * @Flow\Inject
	 */
	protected $categoryRepository;
	
	/**
	 * User
	 *
	 * @var \One50\Shop\Domain\Model\User
	 */
	protected $user;
	
	/**
	 * Assign the users cart items to the view
	 */
	protected function initializeView(\TYPO3\Flow\Mvc\View\ViewInterface $view) {
		// assign categories to view
		$this->view->assign('categories', $this->categoryRepository->findAll());
		
		// assign cart to view
		$view->assign('cart', $this->cart);
		
		// assign user to view (if available)
		$account = $this->securityContext->getAccount();
		if ($account instanceof Account) {
			$this->user = $this->partyService->getAssignedPartyOfAccount($account);
			$this->view->assign('user', $this->user);
		}
	}
}
