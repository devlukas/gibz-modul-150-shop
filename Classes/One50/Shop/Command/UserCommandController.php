<?php
namespace One50\Shop\Command;

/*
 * This file is part of the One50.Shop package.
 */

use One50\Shop\Domain\Model\User;
use TYPO3\Flow\Annotations as Flow;

/**
 * @Flow\Scope("singleton")
 */
class UserCommandController extends \TYPO3\Flow\Cli\CommandController {
	
	/**
	 * @var \TYPO3\Flow\Security\AccountFactory
	 * @Flow\Inject
	 */
	protected $accountFactory;
	
	/**
	 * @var \TYPO3\Flow\Security\Cryptography\HashService
	 * @Flow\Inject
	 */
	protected $hashService;
	
	/**
	 * Account Repository
	 *
	 * @var \TYPO3\Flow\Security\AccountRepository
	 * @Flow\Inject
	 */
	protected $accountRepository;
	
	/**
	 * Person Repository
	 *
	 * @var \One50\Shop\Domain\Repository\UserRepository
	 * @Flow\Inject
	 */
	protected $userRepository;
	
	/**
	 * Create a user
	 *
	 * Create a new user with basic information as email address, password, full name and role. Use this command to
	 * kickstart a new user.
	 *
	 * @param string  $username Username for account
	 * @param string  $password  Password for users account
	 * @param boolean $isAdmin   Wether admin role should be assigned to new users account
	 * @param string  $firstName first name
	 * @param string  $lastName  last name
	 */
	public function createCommand($username, $password, $isAdmin, $firstName, $lastName) {
		$role = $isAdmin ? User::USER_ROLE_ADMIN : User::USER_ROLE_CUSTOMER;
		$account = $this->accountFactory->createAccountWithPassword($username, $password, array($role));
		$this->accountRepository->add($account);
		
		$user = new User();
		$user->setFirstName($firstName);
		$user->setLastName($lastName);
		
		$user->addAccount($account);
		$this->userRepository->add($user);
		
		$this->outputLine('New user and account were successfully created.');
	}
	
}
